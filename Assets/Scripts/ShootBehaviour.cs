﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBehaviour : MonoBehaviour {

    public Transform target;
    private TurretProperty property;
    private GameObject bullet;
    float bulletSpeed;
    Transform muzzle;

	// Use this for initialization
	void Start () {
        property = GetComponent<TurretProperty>();
        bullet = property.bullet;
        bulletSpeed = bullet.GetComponent<BulletProperty>().speed;
        muzzle = property.muzzle;
	}

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 newBulletPos = muzzle.position;
            GameObject outBullet;

            Vector3 tempTargetPos = new Vector3(target.position.x, target.position.y + 0.2f, target.position.z);

            Vector3 aimDirection = tempTargetPos - muzzle.position;
            aimDirection.Normalize();

            outBullet = Instantiate(bullet, newBulletPos, Quaternion.LookRotation(aimDirection));
            Rigidbody rb = outBullet.GetComponent<Rigidbody>();
            rb.AddForce(aimDirection * bulletSpeed);
            Destroy(outBullet, 2);

        }
    }
}
