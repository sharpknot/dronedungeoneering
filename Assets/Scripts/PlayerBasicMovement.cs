﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBasicMovement : MonoBehaviour {

    public float speed = 1f;
    public float topSpeed = 30f;

    private Rigidbody playerRb;
    private Transform playerTr;

    private Vector3 prevPos, currPos;

	// Use this for initialization
	void Start ()
    {
        playerRb = GetComponent<Rigidbody>();
        playerTr = GetComponent<Transform>();

        prevPos = new Vector3(playerTr.position.x, playerTr.position.y, playerTr.position.z);
        currPos = new Vector3(playerTr.position.x, playerTr.position.y, playerTr.position.z);
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }

    private void FixedUpdate()
    {
        // Determine move direction
        Vector3 moveDirection = Vector3.zero;

        moveDirection.x = Input.GetAxis("Horizontal");
        moveDirection.z = Input.GetAxis("Vertical");

        // Check if the move direction is already if a unit vector already. If it is less, then let it be. If more, normalize it
        if (moveDirection.magnitude > 1f)
        {
            moveDirection = Vector3.Normalize(moveDirection);
        }

        // Calculate current speed
        currPos = new Vector3(playerTr.position.x, playerTr.position.y, playerTr.position.z);
        float distance = Vector3.Distance(currPos, prevPos);
        float currSpeed = distance / Time.deltaTime;
        //Debug.Log("Current speed: " + currSpeed);
        prevPos = new Vector3(playerTr.position.x, playerTr.position.y, playerTr.position.z);

        //playerRb.AddRelativeForce(moveDirection * speed);
        
        // Based on the current speed, add thrust to the vehicle
        if (currSpeed <= topSpeed)
        {
            playerRb.AddRelativeForce(moveDirection * speed);
        }
    }
}
