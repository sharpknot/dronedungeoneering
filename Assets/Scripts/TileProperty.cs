﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileProperty : MonoBehaviour {

    public GameObject doorNorth, doorEast, doorSouth, doorWest;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UnlockDoor(int directionIndex)
    {
        switch (directionIndex)
        {
            case 0: Destroy(doorNorth);break;
            case 1: Destroy(doorEast); break;
            case 2: Destroy(doorSouth); break;
            case 3: Destroy(doorWest); break;

        }

    }
}
