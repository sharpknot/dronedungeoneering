﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


enum Direction { NORTH, EAST, SOUTH, WEST};

public class DungeonGenerator : MonoBehaviour {
    
    public GameObject startTile;
    public List<GameObject> tiles;

    public int tileSize;
    public int tileAmount = 1;
    public int seed;

    private Random rand;

	// Use this for initialization
	void Start () {

        // Initialize random class, depending on seed
        Random.InitState(seed);

        // Method: Use drunkard walk algorithm (choose random direction until enough tiles). Connect tiles that are being walked
        int tileCount = 0;
        GridPoint walker = new GridPoint(0, 0); // Create the walker
        GridPoint startTilePoint = new GridPoint(0, 0);   // create the first point, which is the start tile
        
        // List of visited grids
        List<GridPoint> visitedGrid = new List<GridPoint>();
        visitedGrid.Add(startTilePoint);    // add the starting point

        // Current tile
        GridPoint currentPoint = startTilePoint;

        int walkerMoveAmount = 0;
        
        // Walk
        while(tileCount < tileAmount)
        {
            Debug.Log("Walker is at position (" + walker.x + "," + walker.y + "), moved " + walkerMoveAmount + " steps. Current tile amount: " + tileCount);

            int dir = Random.Range(0, 4);   //find random direction; 0,1,2,3 = N,E,S,W

            // Move the walker, open corresponding gate on the previous tile
            switch (dir)
            {
                case 0: // North
                    walker.y = walker.y + 1;
                    break;
                case 1: // East
                    walker.x = walker.x + 1;
                    break;
                case 2: // South
                    walker.y = walker.y - 1;
                    break;
                case 3: // West
                    walker.x = walker.x - 1;
                    break;
            }

            currentPoint.connectDir(dir);   // connect the current tile to the new walker position

            if(pointExist(visitedGrid, walker)) // if the walker's new position is on a point that has been visited
            {
                currentPoint = getPointFromList(visitedGrid, walker);   // set the current point as the visited point
            }
            else
            {
                GridPoint tempPoint  = new GridPoint(walker.x, walker.y);     // create new point based on the walker's position
                visitedGrid.Add(tempPoint);

                currentPoint = tempPoint;   // update the currentPoint
                tileCount++;    // add the visited tile count
            }

            walkerMoveAmount++;

            int oppoDir = (dir + 2) % 4;    // find the opposite direction
            currentPoint.connectDir(oppoDir);   // connect the new point based on the opposite direction

            //Debug.Log("Dir: " + dir + ", Opposite dir: " + oppoDir);

        }

        // Spawn tiles

        foreach(GridPoint g in visitedGrid)
        {
            if(g == startTilePoint) // if it's the starting tile
            {
                GameObject tempStart = Instantiate(startTile, new Vector3(g.x, 0, g.y),Quaternion.identity);
            }
            else
            {
                // Choose random tile in the tile list and spawn it
                GameObject tempTile = Instantiate(tiles[Random.Range(0,tiles.Count)], new Vector3(g.x * tileSize, 0, g.y * tileSize), Quaternion.identity);

                TileProperty tempTileProperty = tempTile.GetComponent<TileProperty>();

                int rotationAmount = Random.Range(0, 4);
                Transform tempTileTransform = tempTile.GetComponent<Transform>();
                tempTileTransform.Rotate(Vector3.down, rotationAmount * 90);  // rotate
                // Open the corresponding gates for each tile (destroying the gates)
                List<int> directionIndexes = g.getDirections();
                foreach(int i in directionIndexes)
                {
                    tempTileProperty.UnlockDoor((i + rotationAmount) % 4);

                }
            }

          
        }
	}

    private bool pointExist(List<GridPoint> pointList, GridPoint point)
    {
        bool output = false;

        foreach(GridPoint g in pointList)
        {
            if(point == g)
            {
                output = true;
                break;
            }
        }

        return output;

    }

    private GridPoint getPointFromList(List<GridPoint> pointList, GridPoint point)
    {
        GridPoint output = new GridPoint(0,0);

        foreach(GridPoint g in pointList)
        {
            if (g == point)
            {
                output = g;
                break;
            }
        }

        return output;
    }
	
}

public class Tile
{
    public Tile()
    {

    }
}

public class GridPoint
{
    public int x, y;
    private List<Direction> connections;
    
    public GridPoint(int x, int y)
    {
        this.x = x;
        this.y = y;

        connections = new List<Direction>();
    }
    
    public void connectNorth()
    {
        if (!connections.Contains(Direction.NORTH))
        {
            connections.Add(Direction.NORTH);
        }
    }

    public void connectSouth()
    {
        if (!connections.Contains(Direction.SOUTH))
        {
            connections.Add(Direction.SOUTH);
        }
    }

    public void connectEast()
    {
        if (!connections.Contains(Direction.EAST))
        {
            connections.Add(Direction.EAST);
        }
    }

    public void connectWest()
    {
        if (!connections.Contains(Direction.WEST))
        {
            connections.Add(Direction.WEST);
        }
    }

    public void connectDir(int directionIndex)
    {
        switch (directionIndex)
        {
            case 0: connectNorth(); break;
            case 1: connectEast(); break;
            case 2: connectSouth(); break;
            case 3: connectWest(); break;
        }
    }

    public List<int> getDirections()
    {
        List<int> outputList = new List<int>();

        foreach(Direction i in connections)
        {
            if(i == Direction.NORTH)
            {
                outputList.Add(0);
            }
            else if (i == Direction.EAST)
            {
                outputList.Add(1);
            }
            else if (i == Direction.SOUTH)
            {
                outputList.Add(2);
            }
            else if (i == Direction.WEST)
            {
                outputList.Add(3);
            }

        }

        return outputList;

    }

    public static bool operator== (GridPoint a, GridPoint b)
    {
        
        if(a.x == b.x && a.y == b.y)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public static bool operator!= (GridPoint a, GridPoint b)
    {

        if (a.x == b.x && a.y == b.y)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
}
