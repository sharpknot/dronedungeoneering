﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastPosition : MonoBehaviour {

    public Camera camera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayHit;

        if(Physics.Raycast(ray, out rayHit,Mathf.Infinity,1<<8))
        {
            GetComponent<Transform>().position = rayHit.point;
        }
	}
}
