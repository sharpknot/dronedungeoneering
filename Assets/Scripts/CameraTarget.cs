﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour {

    public Transform player, target;
    public float playerTargetRatio = 0.2f;


	// Update is called once per frame
	void Update () {
        Vector3 direction = target.position - player.position;

        transform.position = player.position + (direction * playerTargetRatio);
    }
}
